App.Models.User = Backbone.Model.extend({

	login: function(username, password) {
		var self = this;
		Parse.User.logIn(username, password, {
        	success: function(user) {
          		toastr.success("Logged In");
          		self.set("userName", username);
        	},
        	error: function(user, error) {
        		toastr.error(error.message);
        	}
      	});
	},

	isLoggedIn: function() {
		return this.has('userName');
	},

	logout: function(){
		var self = this;
		Parse.User.logOut();
		self.unset("userName");
		toastr.success("Logged Out");
	},

	create: function(firstname, lastname, email, password){
		var pUser = new Parse.User();
  		pUser.set("username", email);
  		pUser.set("password", password);
  		pUser.set("email", email);

  		// other fields can be set just like with Parse.Object
  		pUser.set("firstname", firstname);
  		pUser.set("lastname", lastname);
  		pUser.signUp(null, {
    		success: function(pUser) {
      			toastr.success("successfully created your account\nplease login to get started")
    		},
    		error: function(pUser, error) {
      			// Show the error message somewhere and let the user try again.
      			toastr.error("error: sorry therre was an error creating your account.\nthe server said " + error.message );
    		}
  		});
	}
 
});