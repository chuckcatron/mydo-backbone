App.Views.Contact = Backbone.View.extend({

	events: {
		"click #btncontact": "contact"
	},

	initialize: function(options) {
		_.bindAll(this, "showcontact");
    	options.vent.bind("showcontact", this.showcontact);
		this.vent = options.vent;
		hide = false;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

	showcontact: function(){
    	this.render(false);
    },

    contact: function(){
    	var validator = $("#contactView").kendoValidator().data("kendoValidator");
    	if(validator.validate()){
      		this.model.sendComments($("#contactemail").val().trim(), $("#contactname").val().trim(), $("#contactcomments").val().trim());	          
    	}
    }

});