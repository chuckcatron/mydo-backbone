App.Views.About = Backbone.View.extend({

	events: {
	},

	initialize: function(options) {
		_.bindAll(this, "showAbout");
    	options.vent.bind("showAbout", this.showAbout);
		this.vent = options.vent;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

	showAbout: function(){
    	this.render(false);
    }
 
});