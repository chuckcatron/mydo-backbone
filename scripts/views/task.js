App.Views.Task = Backbone.View.extend({

	events: {
		"click #btnAddTaskItem": "addTaskItem",
		"click #btnUserTaskUpdate": "updateTaskItem",
		"click #btnUserTaskDelete": "deleteTaskItem",
		"click #btnUserTaskComplete": "completeTaskItem"
	},

	initialize: function(options){
		listId = options.listId;
		_.bindAll(this, "showUserTaskView");
    	options.vent.bind("showUserTaskView", this.showUserTaskView, listId);
    	this.vent = options.vent;
    	hide = false;
	},

	render: function(hide, listId){
		console.log('render');
		this.$el.toggleClass("hidden", hide);
		if(hide === false){
	  		TaskData.showTaskItems(listId);
	  		$("#listName").text(ListData.setTaskLabel(listId));
		}
	},

	showUserTaskView: function(listId){
		this.render(false, listId);
	},

	addTaskItem: function(e){
		var validator = $("#addTaskItem").kendoValidator().data("kendoValidator");
  		if(validator.validate()){
    		e.preventDefault();
    		var addTask = $("#addTask");
    		var title = addTask.val();
    		TaskData.addTaskItem(title);
    		addTask.val("");
    	} else {
    		$("#addTask").focus();
  		}
	},

	updateTaskItem: function(e){
		this.hideAddError();
		var validator = $("#userTaskEditItem").kendoValidator().data("kendoValidator");
  		if(validator.validate()){
  			var updateTask = $(e.currentTarget);
  			var objectId = updateTask.data("id");
    		TaskData.updateTaskItem(objectId, $("#editTask").val().trim());
    		toastr.success("Item updated successfully!")
    	} else {
    		$("#editTask").focus();
  		}
	},

	completeTaskItem: function(e){
		this.hideAddError();
  		var completeTask = $(e.currentTarget);
  		var objectId = completeTask.data("id");
    	TaskData.completeTaskItem(objectId);
    	toastr.success("Item updated successfully!")
	},

	deleteTaskItem: function(e){
		this.hideAddError();
		var deleteTask = $(e.currentTarget);
		var objectId = deleteTask.data("id");
		TaskData.deleteTaskItem(objectId);
		this.hideAddError();
	},

	hideAddError: function(){
		$("#addTaskItem").find("span.k-tooltip-validation").hide();
	}
});