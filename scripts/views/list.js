App.Views.List = Backbone.View.extend({

	events: {
		"click #btnAddListShort": "addListItem",
		"click #btnUserListUpdate": "updateListItem",
		"click #btnUserListDelete": "deleteListItem",
		"click #btnUserListView": "showUserTaskView"
	},

	initialize: function(options){
		_.bindAll(this, "showUserListView");
    	options.vent.bind("showUserListView", this.showUserListView);
    	this.vent = options.vent;
    	hide = false;
	},

	render: function(hide){
		console.log('render');
		this.$el.toggleClass("hidden", hide);
		if(hide === false){
	  		ListData.showListItems();
		}
	},

	showUserListView: function(){
		this.render(false);
	},

	showUserTaskView: function(e){
		var list = $(e.currentTarget);
  		var objectId = list.data("id");
  		this.vent.trigger("showUserTaskView", objectId);
	},

	addListItem: function(e){
		var validator = $("#addListItem").kendoValidator().data("kendoValidator");
  		if(validator.validate()){
    		e.preventDefault();
    		var addList = $("#addListShort");
    		var title = addList.val();
    		ListData.addListItem(title);
    		addList.val("");
    	} else {
    		$("#addListShort").focus();
  		}
	},

	updateListItem: function(e){
		this.hideAddError();
		var validator = $("#userListEditItem").kendoValidator().data("kendoValidator");
  		if(validator.validate()){
  			var updateList = $(e.currentTarget);
  			var objectId = updateList.data("id");
    		ListData.updateListItem(objectId, $("#editList").val().trim());
    		toastr.success("Item updated successfully!")
    	} else {
    		$("#editList").focus();
  		}
	},

	deleteListItem: function(e){
		this.hideAddError();
		var deleteList = $(e.currentTarget);
		var objectId = deleteList.data("id");
		ListData.deleteListItem(objectId);
		this.hideAddError();
	},

	hideAddError: function(){
		$("#addListItem").find("span.k-tooltip-validation").hide();
	}
});