App.Views.CreateAccount = Backbone.View.extend({

	events: {
		"click #btncreateaccount": "create"
	},

	initialize: function(options) {
		_.bindAll(this, "createaccount");
    	options.vent.bind("createaccount", this.createaccount);
    	this.vent = options.vent;
    	hide = false;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

  createaccount: function(){
  	this.render(false);
  },

  create: function(){
  	var validator = $("#createView").kendoValidator({
                    rules: {
                        match: function(input){
                            if (input.is("#accountconfirmpassword")) {
                              return input.val() === $("#accountpassword").val();
                            }
                            return true;
                        }
                      },
                      messages: {
                            match: "password and confirm password must match"    
                      }
                    }).data("kendoValidator");
		if(validator.validate()){
  		var firstname = $("#accountfirst").val().trim(),
				lastname = $("#accountlast").val().trim(),
				email = $("#accountemail").val().trim(),
				password = $("#accountpassword").val().trim(),
				confirmpassword = $("#accountconfirmpassword").val().trim();
		this.model.create(firstname, lastname, email, password)
		this.vent.trigger("showlogin", this.model);
  	}
  }
 
});