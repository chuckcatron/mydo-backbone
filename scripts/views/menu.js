App.Views.Menu = Backbone.View.extend({

	events: {
		"click #btnNavHome": "home",
		"click #btnNavAbout": "about",
		"click #btnNavCreateAccount": "createaccount",
		"click #btnNavLogin": "login",
		"click #btnNavLogout": "logout",
		"click #btnNavContact": "contact",
		"click #btnNavMyStuff": "mystuff"
	},

	initialize: function(options){
		this.render();
		this.model.on("change", this.render, this);
		this.vent = options.vent;
	},

	render: function(){
		var showMenuOptions = this.model.isLoggedIn();
		if(showMenuOptions)
		{
			$("#btnNavLogout").show();
			$("#btnNavLogin").hide();
			$("#btnNavCreateAccount").hide();
			$("#btnNavMyStuff").show();
		}
		else{
			$("#btnNavLogout").hide();
			$("#btnNavLogin").show();
			$("#btnNavCreateAccount").show();
			$("#btnNavMyStuff").hide();
		}
		return this;
	},

	home: function(){
		$("#loginView").toggleClass("hidden", true);
		$("#createView").toggleClass("hidden", true);
		$("#contactView").toggleClass("hidden", true);
		$("#aboutView").toggleClass("hidden", true);
		$("#listUserView").toggleClass("hidden", true);
		$("#taskUserView").toggleClass("hidden", true);
		this.vent.trigger("showHome", this.model);
		this.updateDisplay($("#btnNavHome"));
	},

	about: function(){
		$("#loginView").toggleClass("hidden", true);
		$("#createView").toggleClass("hidden", true);
		$("#contactView").toggleClass("hidden", true);
		$("#homeView").toggleClass("hidden", true);
		$("#listUserView").toggleClass("hidden", true);
		$("#taskUserView").toggleClass("hidden", true);
		this.vent.trigger("showAbout", this.model);
		this.updateDisplay($("#btnNavAbout"));
	},

	createaccount: function(){
		$("#homeView").toggleClass("hidden", true);
		$("#loginView").toggleClass("hidden", true);
		$("#contactView").toggleClass("hidden", true);
		$("#aboutView").toggleClass("hidden", true);
		$("#listUserView").toggleClass("hidden", true);
		$("#taskUserView").toggleClass("hidden", true);
    	this.vent.trigger("createaccount", this.model);
    	this.updateDisplay($("#btnNavCreateAccount"));
    },

    login: function(){
    	$("#homeView").toggleClass("hidden", true);
    	$("#createView").toggleClass("hidden", true);
    	$("#contactView").toggleClass("hidden", true);
    	$("#aboutView").toggleClass("hidden", true);
    	$("#listUserView").toggleClass("hidden", true);
    	$("#taskUserView").toggleClass("hidden", true);
    	this.vent.trigger("showlogin", this.model);
    	this.updateDisplay($("#btnNavLogin"));	
    },

    logout: function(){
    	$("#createView").toggleClass("hidden", true);
    	$("#contactView").toggleClass("hidden", true);
    	$("#homeView").toggleClass("hidden", false);
    	$("#aboutView").toggleClass("hidden", true);
    	$("#listUserView").toggleClass("hidden", true);
    	$("#taskUserView").toggleClass("hidden", true);
    	this.vent.trigger("logout", this.model);
    	this.updateDisplay($("#btnNavHome"));	
    },

    contact: function(){
    	$("#loginView").toggleClass("hidden", true);
    	$("#createView").toggleClass("hidden", true);
    	$("#homeView").toggleClass("hidden", true);
    	$("#aboutView").toggleClass("hidden", true);
    	$("#listUserView").toggleClass("hidden", true);
    	$("#taskUserView").toggleClass("hidden", true);
    	this.vent.trigger("showcontact", this.model);
    	this.updateDisplay($("#btnNavContact"));	
    },
    
    mystuff: function(){
    	$("#loginView").toggleClass("hidden", true);
    	$("#createView").toggleClass("hidden", true);
    	$("#homeView").toggleClass("hidden", true);
    	$("#aboutView").toggleClass("hidden", true);
    	$("#listUserView").toggleClass("hidden", true);
    	$("#taskUserView").toggleClass("hidden", true);
    	this.vent.trigger("showUserListView", this.model);
    	this.updateDisplay($("#btnNavMyStuff"));	
    },

    updateDisplay: function(btn){
    	$('#navUL').children().removeClass("active");
  		$(btn).parent().addClass('active');
    }
});