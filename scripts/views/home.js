App.Views.Home = Backbone.View.extend({

	events: {
	},

	initialize: function(options) {
		_.bindAll(this, "showHome");
    	options.vent.bind("showHome", this.showHome);
		this.vent = options.vent;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

	showHome: function(){
    	this.render(false);
    }
 
});