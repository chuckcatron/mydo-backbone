App.Views.Login = Backbone.View.extend({

	events: {
		"click #btnsignin": "login",
		"click #createaccount": "createaccount"
	},

	initialize: function(options) {
		_.bindAll(this, "showlogin");
		_.bindAll(this, "logout");
    	options.vent.bind("showlogin", this.showlogin);
    	options.vent.bind("logout", this.logout);
		this.model.on("change", this.render, this);
		this.vent = options.vent;
		hide = false;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

	login: function() {
		var validator = $("#loginView").kendoValidator().data("kendoValidator");
    	if(validator.validate()){
			this.model.login($("#username").val().trim(), $("#password").val().trim());
			$("#username").val('');
			$("#password").val('');
			this.vent.trigger("showUserListView", this.model);
		}
    },

    createaccount: function(){
    	this.$el.toggleClass("hidden", true);
    	this.vent.trigger("createaccount", this.model);
    },

    showlogin: function(){
    	$("#createView").toggleClass("hidden", true);
    	this.render();
    },

    logout: function(){
    	this.model.logout();
    	this.render();
    }
 
});