$(function() {
	Parse.initialize("GKUNuUaCJamW87YUZwFPpwWGBWlRRII0uLDuFYZv", "bajvKorzBS0t4ITR8ERFrm0Ka8nAbDtHrqSuAW5W");
	var vent = _.extend({}, Backbone.Events);

	App.currentUser = new App.Models.User();
	App.contact = new App.Models.Contact();
	
	var homeView = new App.Views.Home({el: $('#homeView'), model: App.currentUser, vent: vent});
	var aboutView = new App.Views.About({el: $('#aboutView'), model: App.currentUser, vent: vent});
	var loginView = new App.Views.Login({el: $('#loginView'), model: App.currentUser, vent: vent});
	var menuView = new App.Views.Menu({el: $('#menuView'), model: App.currentUser, vent: vent});
	var createAccountView = new App.Views.CreateAccount({el: $('#createView'), model: App.currentUser, vent: vent});
	var contactView = new App.Views.Contact({el: $('#contactView'), model: App.contact, vent: vent});
	var listView = new App.Views.List({el: $('#listUserView'), model: App.currentUser, vent: vent});
	var taskView = new App.Views.Task({el: $('#taskUserView'), model: App.currentUser, vent: vent, listId: 0});


	homeView.render(false);
	aboutView.render(true);
	loginView.render(true);
	createAccountView.render(true);
	contactView.render(true);
	listView.render(true);
	taskView.render(true);

	

	var statusdata = [
				{ text: "all tasks", value: "all" },
                { text: "not done", value: "false" },
                { text: "done", value: "true" }
             ];
  	$("#status").kendoDropDownList({
    	         	dataTextField: "text",
        	     	dataValueField: "value",
            	 	dataSource: statusdata,
               	 	index: 0,
                	change: onStatusChange
  	});

  	function onStatusChange(){
	    var status = $("#status").val();
	    var stat = false;
	    if(status === "true")
	      stat = true;
	  	else if(status === "all")
	  		stat = null;
	  	else
	  		stat = false;

	    TaskData.filterTasks(stat);
  	}
});

var lastListId = 2,
	lastTaskId = 2,
	baseUrl = "https://api.parse.com/1/classes",
	headers = { "X-Parse-Application-Id": "GKUNuUaCJamW87YUZwFPpwWGBWlRRII0uLDuFYZv", "X-Parse-REST-API-Key": "cPgH6lgPaGk7KpVhwhXekwYjVtV6hKj5lltiClx3" };

var	ListData = (function(){
	var userListTemplate = kendo.template($("#listTemplate").html()),
		editListTemplate = kendo.template($("#listEditTemplate").html()),

		ListItem = kendo.data.Model.define({
						id: "objectId",
						fields: {
  							/*objectId:{ readonly: true, nullable: true },*/
    						listid: {},      
    						title: {},
    						userId: {},
    						createdAt: {},
    						done: {}
  						}
		}),
	
		userListDataSource = new kendo.data.DataSource({
	    transport: {
	      read: {
	        url: baseUrl + '/ListItems',
	        headers: headers,
	        dataType: "json"
	      },
	      update: {
	        url: function(params){
	          return baseUrl + '/ListItems' + "/" + params.objectId;
	        },
	        headers: headers,
	        contentType: "application/json",
	        dataType: "json",
	        type: "PUT"
	      },
	      create: {
	        url: baseUrl + '/ListItems',
	        headers: headers,
	        contentType: "application/json",
	        dataType: "json",
	        processData: false,
	        type: "POST"
	      },
	      destroy: {
	        url: function(params){
	          return baseUrl + '/ListItems' + "/" + params.objectId;
	        },
	        headers: headers,
	        contentType: "application/json",
	        dataType: "json",
	        type: "DELETE"
	      },
	      parameterMap: function (data, operation) {
	          if(operation === "read"){
	            console.log('read');
	            //console.log(Parse.User.current().id);
	            //var rs = 'where=' + kendo.stringify( { userId : Parse.User.current().id } );
	            var rs = 'where=' + kendo.stringify( { userId : 'yDOClyCzIi' } );
	            return rs
	          }
	          else if(operation === "create"){
	            console.log('create ' + li.title);
	            li.set("userId", Parse.User.current().id);
	              return kendo.stringify(li);
	          }
	          else if(operation === "update"){
	            console.log('update ' + li.title);
	            return kendo.stringify(li);
	          }
	          else if(operation === "destroy"){
	            console.log('destroy');
	          }
	      }
	    },
	    sort: { field: "createdAt", dir: "desc" },
	    schema: {
	      model: ListItem,
	      data: "results"
	    },
	    error: function(e) {
	      toastr.error(e);
		}
	});

	// When the data source changes, render the items
	userListDataSource.bind("change", function(e) { 
	  var html = kendo.render(userListTemplate, this.view());
	});

	var showListItems = function(){
		$("#userLists").kendoListView({ 
	    	dataSource: userListDataSource,
	    	template: userListTemplate,
	    	editTemplate: editListTemplate,
	    	selectable: false,
	    	refreshTemplate: "Reloading...",
  		});
	},

	setTaskLabel = function(objectId){
		item = userListDataSource.get(objectId);
		var title = item.get("title");
		return title;
	},

	addListItem = function(title){
		lastListId += 1;
		li = new ListItem({
  			listid: lastListId,
  			done: false,
  			title: title
		});
    	userListDataSource.add(li);
    	userListDataSource.sync();
    	//userListDataSource.read();
		//loadUserLists();
		//$("#task-userview").hide();
	},

	updateListItem = function(id, title){
		var objectId = id,
		item = userListDataSource.get(objectId);
		item.set("title", title);
		li = item;
		userListDataSource.sync();
	},

	deleteListItem = function(id){
		var objectId = id,
		item = userListDataSource.get(objectId);
		userListDataSource.remove(item);
		userListDataSource.sync();
	},

	dataSource = function(){
		return userListDataSource;
	};

	return {
		showListItems: showListItems,
		addListItem: addListItem,
		updateListItem: updateListItem,
		deleteListItem: deleteListItem,
		setTaskLabel: setTaskLabel
	}
})();

var	TaskData = (function(){
	var userTaskTemplate = kendo.template($("#taskTemplate").html()),
		
		editTaskTemplate = kendo.template($("#taskEditTemplate").html()),

		TaskItem = kendo.data.Model.define({
				   	id: "objectId",
				   	fields: {
				    	/*objectId:{ readonly: true, nullable: true },*/
				       	listid: {},      
				       	title: {},
				       	createdAt: {},
				       	done: {}	
	      			}
		}),
		
		selectedTaskId = 0,
		filterStat = null,

		userTaskDataSource = new kendo.data.DataSource({
        transport: {
          read: {
            url: baseUrl + '/TaskItems',
            headers: headers,
            dataType: "json"
          },
          update: {
            url: function(params){
              return baseUrl + '/TaskItems' + "/" + params.objectId;
            },
            headers: headers,
            contentType: "application/json",
            dataType: "json",
            type: "PUT"
          },
          create: {
            url: baseUrl + '/TaskItems',
            headers: headers,
            contentType: "application/json",
            dataType: "json",
            processData: false,
            type: "POST"
          },
          destroy: {
            url: function(params){
              return baseUrl + '/TaskItems' + "/" + params.objectId;
            },
            headers: headers,
            contentType: "application/json",
            dataType: "json",
            type: "DELETE"
          },
          parameterMap: function (data, operation) {
              if(operation === "read"){
                console.log('read');
                var rs = 'where=' + kendo.stringify( { listid : selectedListId } );
                return rs
              }
              else if(operation === "create"){
                console.log('create ' + ti.title);
                ti.set("listid", selectedListId);
                  return kendo.stringify(ti);
              }
              else if(operation === "update"){
                console.log('update ' + ti.title);
                return kendo.stringify(ti);
              }
              else if(operation === "destroy"){
                console.log('destroy');
              }
            }
        },
        sort: { field: "createdAt", dir: "desc" },
        schema: {
          model: TaskItem,
          data: "results"
        },
        error: function(e) {
          console.log(e);
    	}
	});

	// When the data source changes, render the items
	userTaskDataSource.bind("change", function(e) { 
  		var html = kendo.render(userTaskTemplate, this.view());
	});

	var showTaskItems = function(listId){
		selectedListId = listId;
		$("#userTasks").kendoListView({ 
	    	dataSource: userTaskDataSource,
	    	template: userTaskTemplate,
	    	editTemplate: editTaskTemplate,
	    	selectable: false,
	    	refreshTemplate: "Reloading...",
  		});
	};

	var filterTasks = function(stat){
		filterStat = stat;
		if(stat !== null){
			userTaskDataSource.filter([
		      { field: "done", operator: "eq", value: stat }
		    ]);
		} else {
			userTaskDataSource.filter({});
		}
	};

	var addTaskItem = function(title){
    	lastTaskId += 1;
    	ti = new TaskItem({
      		listid: selectedListId,
      		done: false,
      		title: title
    	});
	    userTaskDataSource.add(ti);
	    userTaskDataSource.sync();
	};

	var updateTaskItem = function(id, title){
		var objectId = id;
  		var item = userTaskDataSource.get(objectId);
  		item.set("title", title);
  		ti = item;
  		userTaskDataSource.sync();
	};

	var completeTaskItem = function(id){
		var objectId = id;
  		var item = userTaskDataSource.get(objectId);
  		var stat = item.get("done");
  		item.set("done", !stat);
  		ti = item;
  		userTaskDataSource.sync();
  		filterTasks(filterStat);
	};

	var deleteTaskItem = function(id){
		var objectId = id;
  		var item = userTaskDataSource.get(objectId);
  		userTaskDataSource.remove(item);
  		userTaskDataSource.sync();
	};

	var dataSource = function(){
		return userTaskDataSource;
	};

	return {
		showTaskItems: showTaskItems,
		addTaskItem: addTaskItem,
		updateTaskItem: updateTaskItem,
		deleteTaskItem: deleteTaskItem,
		completeTaskItem: completeTaskItem,
		filterTasks: filterTasks	
	}
})();